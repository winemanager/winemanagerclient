import {API_DEL_FAILURE, API_DEL_REQUEST, API_DEL_SUCCESS} from "../actions/deleteActions";

const initialState = {
  fetching: false,
  deleted: null,
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case API_DEL_REQUEST:
      return {...state, fetching: true, error: null, id: action.id, resource: action.resource};
    case API_DEL_SUCCESS:
      return {...state, fetching: false, deleted: true, error: null};
    case API_DEL_FAILURE:
      return {...state, fetching: false, deleted: false, error: action.error};
    default:
      return {...state, ...initialState};
  }
};
