import {combineReducers} from 'redux';
import cavernsReducers from '../components/Cavern/reducers/index';
import deleteReducer from './DeleteReducer';
import { reducer as formReducer } from 'redux-form'


export default combineReducers({
  ...cavernsReducers, delete:deleteReducer, form: formReducer
});
