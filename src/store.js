import 'regenerator-runtime/runtime'
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/sagas';

const composeEnhancers = composeWithDevTools({
  // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(rootReducer, /* preloadedState, */
  composeEnhancers(
    applyMiddleware(...[thunk, sagaMiddleware]),
    // other store enhancers if any
  ));

// then run the saga
sagaMiddleware.run(rootSaga);
