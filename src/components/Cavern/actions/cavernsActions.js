export const API_CALL_REQUEST_CAVERNS = "API_CALL_REQUEST_CAVERNS";
export const API_CALL_SUCCESS_CAVERNS = "API_CALL_SUCCESS_CAVERNS";
export const API_CALL_FAILURE_CAVERNS = "API_CALL_FAILURE_CAVERNS";

export const API_CALL_CREATE_CAVERN = "API_CALL_CREATE_CAVERN";
export const API_CALL_CREATE_SUCCESS_CAVERN = "API_CALL_CREATE_SUCCESS_CAVERN";

export const API_CALL_EDIT_CAVERN = "API_CALL_EDIT_CAVERN";
export const API_CALL_EDIT_SUCCESS_CAVERN = "API_CALL_EDIT_SUCCESS_CAVERN";

export function fetchCavernsSuccess(payload) {
  return {type: API_CALL_SUCCESS_CAVERNS, caverns: payload.caverns};
}

export function fetchCaverns(payload) {
  return {type: API_CALL_REQUEST_CAVERNS, payload};
}

export function fetchCavernsFailure(payload) {
  return {type: API_CALL_FAILURE_CAVERNS, payload};
}

export function createCavern(payload) {
  return {type: API_CALL_CREATE_CAVERN, payload};
}

export function createCavernSuccess(payload) {
  return {type: API_CALL_CREATE_SUCCESS_CAVERN, payload};
}

export function editCavern(payload) {
  return {type: API_CALL_EDIT_CAVERN, payload};
}

export function editCavernSuccess(payload) {
  return {type: API_CALL_EDIT_SUCCESS_CAVERN, payload};
}
