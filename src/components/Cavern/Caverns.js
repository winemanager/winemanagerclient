import React from 'react';
import {connect} from 'react-redux';
import compose from 'recompose/compose';
import {bindActionCreators} from 'redux';
import {fetchCaverns} from "./actions/cavernsActions";
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";
import ListItemText from "@material-ui/core/es/ListItemText/ListItemText";
import DeleteButtonModal from './actions_button/DeleteButtonModal';
import {CAVERNS} from "../../utils/api-ressource";
import ListItemSecondaryAction from "@material-ui/core/es/ListItemSecondaryAction/ListItemSecondaryAction";
import withStyles from "@material-ui/core/es/styles/withStyles";
import PropTypes from 'prop-types';
import AddCavernModal from "./actions_button/AddCavernModal";
import EditCavernModal from "./actions_button/EditCavernModal";
import Redirect from "react-router-dom/es/Redirect";

const styles = theme => ({
  list: {
    padding: 0,
    marginBottom: theme.spacing.unit * 8,
  },
  textList: {
    maxWidth: '50vw',
    overflow: 'hidden'
  },
  action: {
    display: 'flex'
  }

});

class Caverns extends React.Component {

  constructor() {
    super();
    this.state = {
      redirect: false
    }
  }


  handleClick = (e, id) => {
    this.setState({redirect: true, cavernId: id});
  };

  redirect = () => {
    if (this.state.redirect) {
      return <Redirect push to={{
        pathname:`cavern/${this.state.cavernId}/wines`,
      }}/>
    }
  };

  renderCavernsList() {
    if (this.props.caverns) {
      return this.props.caverns.map((cavern) => (
          <ListItem key={cavern.id} button onClick={((e) => this.handleClick(e, cavern.id))}>
            <ListItemText className={this.props.classes.textList} textList primary={cavern.name}/>
            <ListItemSecondaryAction className={this.props.classes.action}>
              <EditCavernModal cavernName={cavern.name} cavernId={cavern.id}/>
              <DeleteButtonModal id={cavern.id} resource={CAVERNS}/>
            </ListItemSecondaryAction>
          </ListItem>
        )
      )
    }
  }

  componentDidMount() {
    this.props.fetchCaverns();
  }


  componentDidUpdate(prevProps, prevState) {
    //refresh list
    if (this.props.location !== prevProps.location) {
      this.props.fetchCaverns();
    }
  }

  render() {
    const {classes} = this.props;
    return (
      <div>
        {this.redirect()}
        <List className={classes.list}
              component="nav"
        >
          {this.renderCavernsList()}
        </List>
        <AddCavernModal/>
      </div>
    )
  }
}

Caverns.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const enhance = compose(
  connect(
    (state, props) => ({
      caverns: state.caverns.caverns,
      ...props
    }),
    dispatch => ({
      fetchCaverns: bindActionCreators(fetchCaverns, dispatch),
    })
  ),
  withStyles(styles, {withTheme: true})
);

export default enhance(Caverns);

