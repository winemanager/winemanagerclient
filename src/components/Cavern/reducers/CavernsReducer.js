// reducer with initial state
import {
  API_CALL_CREATE_CAVERN,
  API_CALL_CREATE_FAILURE_CAVERN,
  API_CALL_CREATE_SUCCESS_CAVERN,
  API_CALL_EDIT_CAVERN,
  API_CALL_EDIT_SUCCESS_CAVERN,
  API_CALL_FAILURE_CAVERNS,
  API_CALL_REQUEST_CAVERNS,
  API_CALL_SUCCESS_CAVERNS
} from "../actions/cavernsActions";

const initialState = {
  fetching: false,
  caverns: null,
  cavern: null,
  added: null,
  edited: null,
  cavernId: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case API_CALL_REQUEST_CAVERNS:
      return {...state, fetching: true, added: false, edited: false};
    case API_CALL_SUCCESS_CAVERNS:
      return {...state, fetching: false, caverns: action.caverns};
    case API_CALL_FAILURE_CAVERNS:
      return {...state, fetching: false, caverns: null};
    case API_CALL_CREATE_CAVERN:
      return {...state, fetching: true, cavern: action.cavern};
    case API_CALL_CREATE_SUCCESS_CAVERN:
      return {...state, fetching: false, added: true, cavern: action.cavern};
    case API_CALL_EDIT_CAVERN:
      return {...state, fetching: true, cavern: action.cavern};
    case API_CALL_EDIT_SUCCESS_CAVERN:
      return {...state, fetching: false, edited: true, cavernId: action.cavernId};
    default:
      return state;
  }
};
