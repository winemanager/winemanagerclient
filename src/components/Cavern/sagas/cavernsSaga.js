import {call, put, takeEvery} from 'redux-saga/effects'
import {
  API_CALL_CREATE_CAVERN,
  API_CALL_EDIT_CAVERN,
  API_CALL_REQUEST_CAVERNS,
  createCavernSuccess,
  editCavernSuccess,
  fetchCavernsFailure,
  fetchCavernsSuccess
} from "../actions/cavernsActions";
import {CAVERNS} from "../../../utils/api-ressource";
import {edit, get, post} from "../../../utils/api";
import {stopSubmit} from "redux-form";

function* fetchCaverns(action) {
  try {
    let caverns = yield call(get, `/${CAVERNS}`);
    yield put(fetchCavernsSuccess({caverns: caverns}));
  } catch (e) {
    yield put(fetchCavernsFailure({message: e.message}));
  }
}

function* createCavern(action) {
  try {
    let cavern = yield call(post, `/${CAVERNS}`, {name: action.payload.cavern});
    yield put(createCavernSuccess({cavern: cavern}));
  } catch (e) {
    let errors = {};
    e.errors.forEach((value) => {
      let a = {};
      a[value.propertyPath] = value.message;
      errors = {...errors, ...a};
    });
    yield put(stopSubmit('cavernCRUD', errors))
  }
}

function* editCavern(action) {
  try {
    let cavernId = yield call(edit, `/${CAVERNS}`, action.payload.cavernId, {name: action.payload.name});
    yield put(editCavernSuccess({cavernId: cavernId}));
  } catch (e) {
    let errors = {};
    e.errors.forEach((value) => {
      let a = {};
      a[value.propertyPath] = value.message;
      errors = {...errors, ...a};
    });
    yield put(stopSubmit('cavernCRUD', errors))
  }
}


export function* cavernsSaga() {
  yield takeEvery(API_CALL_REQUEST_CAVERNS, fetchCaverns);
}

export function* createCavernSaga() {
  yield takeEvery(API_CALL_CREATE_CAVERN, createCavern);
}

export function* editCavernSaga() {
  yield takeEvery(API_CALL_EDIT_CAVERN, editCavern);
}

