import React from "react";
import {Redirect} from "react-router";
import {CAVERNS} from "../../../utils/api-ressource";
import EditIcon from '@material-ui/icons/Edit';
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import compose from "recompose/compose";
import {connect} from "react-redux";
import CRUDModal, {EDITED} from "./CRUDModal";


class EditCavernModal extends React.Component {

  constructor() {
    super();
    this.state = {
      openModal: false,
    }
  }

  handleClick = (event) => {
    this.setState({openModal: true});
    event.stopPropagation();
  };

  componentWillUpdate(nextProps) {
    if (nextProps.edited !== this.props.edited && nextProps.edited) {
      this.setState({openModal: false});
    }
  }

  render() {

    return (
      this.props.edited ?
        <Redirect push to={{
          pathname: `${CAVERNS}`.split("/")[1],
          state: {refresh: true}
        }}/>
        :
        !this.state.openModal ?
          <IconButton onClick={this.handleClick}>
            <EditIcon/>
          </IconButton>
          : <div>
            <CRUDModal CRUDType={EDITED} cavernId={this.props.cavernId} cavernName={this.props.cavernName}
                       openModal={this.state.openModal}/>
            <IconButton onClick={this.handleClick}>
              <EditIcon/>
            </IconButton>
          </div>
    )
  }
}

const enhance = compose(
  connect(
    (state, props) => {
      return ({
        edited: state.caverns.edited,
      })
    },
  )
);

export default enhance(EditCavernModal);
