import React from 'react';
import Typography from "@material-ui/core/es/Typography/Typography";
import Modal from "@material-ui/core/es/Modal/Modal";
import withStyles from "@material-ui/core/es/styles/withStyles";
import PropTypes from 'prop-types';
import Button from "@material-ui/core/es/Button/Button";
import Close from '@material-ui/icons/Close';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import compose from "recompose/compose";
import {Redirect} from "react-router";
import {createCavern, editCavern} from "../actions/cavernsActions";
import EditIcon from '@material-ui/icons/Edit';
import {Field, reduxForm} from 'redux-form';
import TextField from '@material-ui/core/TextField';


export const EDITED = 'EDITED';
export const ADDED = 'ADDED';
export const renderTextField = ({meta: {touched, error} = {}, input: {value, ...inputProps}, ...props}) => {
    return (
      <TextField
        error={!!(touched && error)}
        helperText={touched && error}
        defaultValue={value}
        {...inputProps}
        {...props}
      />
    )
  }
;

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    [theme.breakpoints.down('xs')]: {
      width: '80vw',
    },
    [theme.breakpoints.up('sm')]: {
      width: '50vw',
    },
    [theme.breakpoints.up('md')]: {
      width: '35vw',
    },
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: '30px 20px 30px 20px',
    outline: 'none',
    display: "flex",
    flexDirection: 'column',
    alignItems: 'center',
  },
  add: {
    fontSize: '10px',
    width: '100px',
    margin: theme.spacing.unit,
  },
  cancel: {
    fontSize: '10px',
    width: '100px',
    margin: theme.spacing.unit
  },
  icon: {
    fontSize: '19px'
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    alignSelf: 'flex-start'
  },
  form: {
    margin: theme.spacing.unit,
    width: '100%'
  },
  field: {
    margin: theme.spacing.unit,
  },
  title: {
    margin: theme.spacing.unit
  }
});

class CRUDModal extends React.Component {

  constructor(props) {
    super();
    this.state = {
      openModal: props.openModal,
    }
  }

  handleClose = () => {
    this.setState({openModal: false});
  };

  onSubmit = (values) => {
    switch (this.props.CRUDType) {
      case EDITED:
        return this.props.CRUDFunc({cavernId: this.props.cavernId, name: values.name});
      case ADDED:
        return this.props.CRUDFunc({cavern: values.name});
    }
  };

  renderTilte(type) {
    switch (type) {
      case EDITED:
        return "Edition d'une cave";
      case ADDED:
        return "Création d'une cavern";
    }
  }

  renderButton(type) {
    switch (type) {
      case EDITED:
        return "Modifier";
      case ADDED:
        return "Ajouter";
    }
  }

  render() {
    const {classes, handleSubmit} = this.props;
    return (<Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={this.state.openModal}
        onClose={this.handleClose}
      >
        <div style={getModalStyle()} className={classes.paper}>
          <Typography variant="h6" id="modal-title" className={classes.title}>
            {
              this.renderTilte(this.props.CRUDType)
            }
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit(this.onSubmit)}>
            <Field
              className={classes.field}
              fullWidth
              name="name"
              component={renderTextField}
              label="Nom de la cave"
            />
            <div className={classes.buttons}>
              <Button type="submit"
                      variant="contained" color="secondary" className={classes.add}>
                <EditIcon className={classes.icon}/>
                {
                  this.renderButton(this.props.CRUDType)
                }
              </Button>
              <Button onClick={this.handleClose} variant="contained" color="primary" className={classes.cancel}>
                <Close className={classes.icon}/>
                Annuler
              </Button>
            </div>
          </form>
        </div>
      </Modal>
    )
  }
}

CRUDModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

const enhance = compose(
  connect(
    (state, props) => {
      return ({
        initialValues: {name: props.cavernName},
      })
    },
    (dispatch, props) => {
      switch (props.CRUDType) {
        case EDITED:
          return  ({CRUDFunc: bindActionCreators(editCavern, dispatch)});
        case ADDED:
          return  ({CRUDFunc: bindActionCreators(createCavern, dispatch)});
      }
    }
  ),
  reduxForm({
    form: 'cavernCRUD', // a unique identifier for this form
    enableReinitialize: true
  }),
  withStyles(styles)
);

export default enhance(CRUDModal);
