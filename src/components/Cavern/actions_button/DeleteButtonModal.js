import React from "react";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import DeleteForever from '@material-ui/icons/DeleteForever';
import Typography from "@material-ui/core/es/Typography/Typography";
import Modal from "@material-ui/core/es/Modal/Modal";
import withStyles from "@material-ui/core/es/styles/withStyles";
import PropTypes from 'prop-types';
import Button from "@material-ui/core/es/Button/Button";
import DeleteIcon from '@material-ui/icons/Delete';
import Close from '@material-ui/icons/Close';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import compose from "recompose/compose";
import {deleteResource} from "../../../actions/deleteActions";
import {Redirect} from "react-router";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    height: theme.spacing.unit * 20,
    [theme.breakpoints.down('xs')]: {
      width: '95vw',
    },
    [theme.breakpoints.up('sm')]: {
      width: '50vw',
    },
    [theme.breakpoints.up('md')]: {
      width: '30vw',
    },
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: '30px 20px 30px 20px',
    outline: 'none',
    display: "flex",
    alignItems: "flex-end",
    justifyContent: 'space-between'
  },
  title: {
    alignSelf: 'flex-start',
    order: 1
  },
  add: {
    order: 0,
    fontSize: '10px',
    width: '100px'
  },
  cancel: {
    order: 2,
    fontSize: '10px',
    width: '100px'
  },
  icon: {
    fontSize: '19px'
  }
});

class DeleteButtonModal extends React.Component {

  constructor() {
    super();
    this.state = {
      openModal: false
    }
  }

  handleClick = (event) => {
    this.setState({openModal: true});
    event.stopPropagation();
  };


  handleClose = () => {
    this.setState({openModal: false});
  };

  deleteResource = () => {
    this.props.deleteResource({id: this.props.id, resource: this.props.resource});
    this.setState({openModal: false});
  };

  render() {
    const {classes} = this.props;
    return (
      this.props.deleted ?
        <Redirect push to={{
          pathname: `${this.props.resource}`.split("/")[1],
          state: {refresh: true}
        }}/>
        :
        !this.state.openModal ?
          <IconButton onClick={this.handleClick}>
            <DeleteForever/>
          </IconButton>
          :
          <div>
            <IconButton onClick={this.handleClick}>
              <DeleteForever/>
            </IconButton>
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.openModal}
              onClose={this.handleClose}
            >
              <div style={getModalStyle()} className={classes.paper}>
                <Typography variant="h6" id="modal-title" className={classes.title}>
                  Confirmer
                </Typography>
                <Button onClick={this.deleteResource}
                        variant="contained" color="secondary" className={classes.add}>
                  <DeleteIcon className={classes.icon}/>
                  Supprimer
                </Button>
                <Button onClick={this.handleClose} variant="contained" color="primary" className={classes.cancel}>
                  <Close className={classes.icon}/>
                  Annuler
                </Button>
              </div>
            </Modal>
          </div>
    )
  }
}

DeleteButtonModal.propTypes = {
  classes: PropTypes.object.isRequired,
};

const enhance = compose(
  connect(
    (state, props) => ({
      deleted: state.delete.deleted,
    }),
    dispatch => ({
      deleteResource: bindActionCreators(deleteResource, dispatch)
    })
  ),
  withStyles(styles)
);

export default enhance(DeleteButtonModal);
