import React from "react";
import {Redirect} from "react-router";
import {CAVERNS} from "../../../utils/api-ressource";
import EditIcon from '@material-ui/icons/Edit';
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import compose from "recompose/compose";
import {connect} from "react-redux";
import Fab from "@material-ui/core/es/Fab/Fab";
import withStyles from "@material-ui/core/es/styles/withStyles";
import AddIcon from '@material-ui/icons/Add';
import CRUDModal, {ADDED} from "./CRUDModal";

const styles = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  }
});

class AddCavernModal extends React.Component {

  constructor() {
    super();
    this.state = {
      openModal: false,
    }
  }

  handleClick = (event) => {
    this.setState({openModal: true});
    event.stopPropagation();
  };

  componentWillUpdate(nextProps) {
    if (nextProps.added !== this.props.added && nextProps.added) {
      this.setState({openModal: false});
    }
  }

  render() {
    const {classes,} = this.props;
    return (
      this.props.added ?
        <Redirect push to={{
          pathname: `${CAVERNS}`.split("/")[1],
          state: {refresh: true}
        }}/>
        :
        !this.state.openModal ?
          <Fab onClick={this.handleClick} className={classes.fab} color="primary">
            <AddIcon/>
          </Fab>
          : <div>
            <CRUDModal CRUDType={ADDED}
                       openModal={this.state.openModal}/>
            <IconButton onClick={this.handleClick}>
              <EditIcon/>
            </IconButton>
          </div>
    )
  }
}

const enhance = compose(
  connect(
    (state, props) => {
      return ({
        added: state.caverns.added,
      })
    },
  ),
  withStyles(styles)
);

export default enhance(AddCavernModal);
