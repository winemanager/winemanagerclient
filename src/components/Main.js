import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Caverns from "./Cavern/Caverns";
import Cavern from "./Cavern/Cavern";

const Main = () => (
  <main className={"App-main"}>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route exact path='/caverns' component={Caverns}/>
      <Route exact path='/cavern/:id/wines' component={Cavern}/>
    </Switch>
  </main>
);

export default Main
