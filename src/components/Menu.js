import React from "react";
import {Link} from "react-router-dom";
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import ListItemText from "@material-ui/core/es/ListItemText/ListItemText";

class Menu extends React.Component {
  render() {
    return (
      this.props.anchorEl != undefined && this.props.open != undefined && this.props.onClose != undefined ?
      <Menu
        id="long-menu"
        anchorEl={this.props.anchorEl}
        open={this.props.open}
        onClose={this.props.onClose}
        // PaperProps={{
        //   style: {
        //     maxHeight: ITEM_HEIGHT * 4.5,
        //     width: 200,
        //   },
        // }}
      >
        <MenuItem component={Link} to="/">
          <ListItemText inset primary="Accueil"/>
        </MenuItem>
        <MenuItem component={Link} to="/caverns">
          <ListItemText inset primary="Listes des caves"/>
        </MenuItem>
      </Menu>
        : null
    )
  }
}

export default Menu;
