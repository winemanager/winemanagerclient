export const API_DEL_REQUEST = "API_DEL_REQUEST";
export const API_DEL_SUCCESS = "API_DEL_SUCCESS";
export const API_DEL_FAILURE = "API_DEL_FAILURE";
export const API_DEL_RESET = "API_DEL_RESET";


export function deleteResourceSuccess() {
  return {type: API_DEL_SUCCESS, deleted: true};
}

export function deleteResource(payload) {
  return {type: API_DEL_REQUEST, payload};
}

export function deleteResourceFailure(payload) {
  return {type: API_DEL_FAILURE, payload};
}

export function deleteResourceReset(payload) {
  return {type: API_DEL_RESET, payload};
}
