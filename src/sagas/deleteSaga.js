import {call, put, takeEvery} from 'redux-saga/effects'
import {del} from "../utils/api";
import {API_DEL_REQUEST, deleteResourceFailure, deleteResourceSuccess} from "../actions/deleteActions";

function* deleteResource(action) {
  try {
    yield call(del, `/${action.payload.resource}`, action.payload.id);
    yield put(deleteResourceSuccess());
  } catch (e) {
    yield put(deleteResourceFailure({message: e.message}));
  }
}

function* deleteResourceSaga() {
  yield takeEvery(API_DEL_REQUEST, deleteResource);
}

export default deleteResourceSaga;
