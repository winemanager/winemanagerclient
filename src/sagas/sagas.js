import {cavernsSaga, createCavernSaga, editCavernSaga} from "../components/Cavern/sagas/cavernsSaga";

import {all} from 'redux-saga/effects'
import deleteResourceSaga from "./deleteSaga";

export default function* rootSaga() {
  yield all([
    editCavernSaga(),
    createCavernSaga(),
    cavernsSaga(),
    deleteResourceSaga()
  ])
}
