import {API_URL} from "./api-ressource";
import queryString from 'querystring';

const token = "AAA";
export const get = function (endpoint, data) {
  var h = new Headers();
  // const token = localStorage.getItem('token');
  h.append('Authorization', `Bearer ${token}`);
  h.append('accept', 'application/json');
  return fetch(API_URL + endpoint + (data ? '?' + queryString.stringify(data) : ''), {headers: h})
    .then((res) => {
      var text = res.text();
      if (res.status === 200 && text)
        return text.then(txt => JSON.parse(txt));
      else
        return Promise.reject({
          status: res.status,
          error: res.statusText
        });
    }, (reason) => {
      return Promise.reject({
        status: -1,
        error: reason
      });
    });
};

export const del = function (endpoint, data) {
  var h = new Headers();
  // const token = localStorage.getItem('token');
  h.append('Authorization', `Bearer ${token}`);
  h.append('Accept', 'application/json');
  h.append('Content-Type', 'application/json');
  return fetch(API_URL + endpoint + (data ? '/' + data : ''), {method: 'DELETE', headers: h})
    .then((res) => {
      if (res.status === 204)
        return true;
      else
        return Promise.reject({
          status: res.status,
          error: res.statusText
        });
    }, (reason) => {
      return Promise.reject({
        status: -1,
        error: reason
      });
    });
};

export const post = function (endpoint, body) {
  var h = new Headers();
  // const token = localStorage.getItem('token');
  h.append('Authorization', `Bearer ${token}`);
  h.append('Accept', 'application/json');
  h.append('Content-Type', 'application/json');

  return fetch(API_URL + endpoint, {method: 'POST', headers: h, body: JSON.stringify(body)})
    .then((res) =>
      res.json()
        .then(((data) => {
          if (res.status === 200 || res.status === 201) {
            return data
          }
          else {
            return Promise.reject({
              status: res.status,
              errors: data.violations
            })
          }
        }))
    )
};

export const edit = function (endpoint, id, body) {
  var h = new Headers();
  // const token = localStorage.getItem('token');
  h.append('Authorization', `Bearer ${token}`);
  h.append('Accept', 'application/json');
  h.append('Content-Type', 'application/json');

  return fetch(API_URL + endpoint + "/" + id, {
    method: 'PUT',
    headers: h,
    body: JSON.stringify(body)
  })
    .then((res) =>
      res.json()
        .then(((data) => {
          if (res.status === 200) {
            return data
          }
          else {
            return Promise.reject({
              status: res.status,
              errors: data.violations
            })
          }
        }))
    )
};
